package com.bxacosta.seleniumtabs;

import com.bxacosta.seleniumtabs.services.DemoService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@CommonsLog
@SpringBootApplication
public class SeleniumTabsApplication {

    @Autowired
    private DemoService demoService;

    public static void main(String[] args) {
        SpringApplication.run(SeleniumTabsApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent() {
        var games = demoService.getGames();
        log.info(games);
    }
}
