package com.bxacosta.seleniumtabs.controllers;

import com.bxacosta.seleniumtabs.services.DemoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DemoController {

    private final DemoService demoService;

    @GetMapping("/")
    public ResponseEntity<?> start() {
        var games = demoService.getGames();
        return ResponseEntity.ok().body(games);
    }
}
