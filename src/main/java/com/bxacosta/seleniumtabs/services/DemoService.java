package com.bxacosta.seleniumtabs.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@CommonsLog
@RequiredArgsConstructor
public class DemoService {
    private final FirefoxDriver driver;
    private final WebDriverWait wait;

    public List<String> getGames() {
        driver.get("https://www.epicgames.com/store/es-MX/browse");
        var gameList = new ArrayList<String>();
        var games = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(
                By.xpath("//li[@data-component='CatalogItemOfferCard']"), 1));
        games.forEach(game -> {
            var title = game.findElement(By.xpath(".//span[@data-component='OfferTitleInfo']")).getText();
            gameList.add(title);
            if (title.equalsIgnoreCase("Cyberpunk 2077")) {
                clearMemory();
            }
        });
        return gameList;
    }

    public void clearMemory() {
        log.info("Liberando memoria");
        if (driver.getWindowHandles().size() == 1) {
            driver.executeScript("window.open();");
        }
        var tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.get("about:memory");

        var iterations = 3;
        for (int i = 0; i < iterations; i++) {
            driver.executeScript("Array.from(document.querySelectorAll('button')).find(button => button.textContent === 'GC').click();");
            this.sleep(100);
            driver.executeScript("Array.from(document.querySelectorAll('button')).find(button => button.textContent === 'CC').click();");
            this.sleep(100);
            driver.executeScript("Array.from(document.querySelectorAll('button')).find(button => button.textContent === 'Minimize memory usage').click();");
            this.sleep(100);
        }
        driver.close();
        driver.switchTo().window(tabs.get(0));
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignored) {
        }
    }
}
