package com.bxacosta.seleniumtabs.config;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SeleniumConfig {

    @Bean
    public FirefoxDriver getFirefoxDriver() {
        System.setProperty("webdriver.gecko.driver", "D:\\DEVELOP\\drivers\\geckodriver.exe");
        var options = new FirefoxOptions();
//        options.setHeadless(true);
        return new FirefoxDriver(options);
    }

    @Bean
    public WebDriverWait getWebDriverWait(FirefoxDriver driver) {
        return new WebDriverWait(driver, 30);
    }
}
